/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# 转储表 tb_account
# ------------------------------------------------------------

CREATE TABLE `tb_account` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
                              `user_name` varchar(128) DEFAULT NULL COMMENT '登录名',
                              `password` varchar(128) DEFAULT NULL COMMENT '密码',
                              `salt` varchar(32) DEFAULT NULL COMMENT '盐',
                              `nickname` varchar(128) DEFAULT NULL COMMENT '昵称',
                              `email` varchar(64) DEFAULT NULL COMMENT '邮件',
                              `mobile` varchar(32) DEFAULT NULL COMMENT '手机电话',
                              `avatar` varchar(256) DEFAULT NULL COMMENT '账户头像',
                              `status` tinyint(2) DEFAULT NULL COMMENT '状态',
                              `created` datetime DEFAULT NULL COMMENT '创建日期',
                              PRIMARY KEY (`id`) USING BTREE,
                              UNIQUE KEY `email` (`email`) USING BTREE,
                              UNIQUE KEY `mobile` (`mobile`) USING BTREE,
                              KEY `created` (`created`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='账号信息表';

LOCK TABLES `tb_account` WRITE;
/*!40000 ALTER TABLE `tb_account` DISABLE KEYS */;

INSERT INTO `tb_account` (`id`, `user_name`, `password`, `salt`, `nickname`, `email`, `mobile`, `avatar`, `status`, `created`)
VALUES
    (1,'admin','057ab5b6f6209530c0bab4e5a65a95bdb70c5630da22e127b8976386df80d46a','nXeJMHYJLH5Ub903rqIJjOpP','admin','admin@mybatis-flex.com',NULL,NULL,1,'2023-07-07 00:00:00');

/*!40000 ALTER TABLE `tb_account` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 tb_account_session
# ------------------------------------------------------------

CREATE TABLE `tb_account_session` (
                                      `id` varchar(32) NOT NULL DEFAULT '' COMMENT 'session id',
                                      `client` varchar(32) DEFAULT '' COMMENT '客户端',
                                      `account_id` bigint(20) unsigned NOT NULL COMMENT '账户id',
                                      `expire_at` datetime NOT NULL COMMENT '到期时间',
                                      `status` tinyint(2) DEFAULT NULL COMMENT '状态',
                                      `options` text COMMENT '扩展属性',
                                      `created` datetime DEFAULT NULL COMMENT '创建时间',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='账户登录session';



# 转储表 tb_audit_message
# ------------------------------------------------------------

CREATE TABLE `tb_audit_message` (
                                    `id` bigint(20) unsigned NOT NULL COMMENT '主键',
                                    `platform` varchar(32) DEFAULT NULL COMMENT '平台，或者是应用',
                                    `module` varchar(32) DEFAULT NULL COMMENT '模块',
                                    `url` varchar(512) DEFAULT NULL COMMENT 'url id',
                                    `user` varchar(64) DEFAULT NULL COMMENT '平台用户',
                                    `user_ip` varchar(64) DEFAULT NULL COMMENT '平台用户 IP 地址',
                                    `host_ip` varchar(64) DEFAULT NULL COMMENT '服务器 IP',
                                    `query` text COMMENT '执行的 sql',
                                    `query_params` text COMMENT '执行的 sql 的参数',
                                    `query_time` datetime DEFAULT NULL COMMENT '执行时间',
                                    `elapsed_time` int(11) unsigned DEFAULT NULL COMMENT '执行消耗时间',
                                    `created` datetime DEFAULT NULL COMMENT '创建时间',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='审计信息';



# 转储表 tb_audit_message_meta
# ------------------------------------------------------------

CREATE TABLE `tb_audit_message_meta` (
                                         `message_id` bigint(20) unsigned NOT NULL,
                                         `meta_key` varchar(64) DEFAULT NULL,
                                         `meta_value` text,
                                         KEY `message_id` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='审计元信息';



# 转储表 tb_sys_option
# ------------------------------------------------------------

CREATE TABLE `tb_sys_option` (
                                 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                                 `key` varchar(32) NOT NULL DEFAULT '' COMMENT '配置key',
                                 `value` text COMMENT '配置内容',
                                 `created` datetime DEFAULT NULL COMMENT '创建时间',
                                 `modified` datetime DEFAULT NULL COMMENT '修改时间',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='系统配置表';




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
