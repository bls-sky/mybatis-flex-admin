/**
 * Copyright (c) 2022-2023, Mybatis-Flex (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mybatisflex.admin.spring.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class BaseApp  implements ApplicationListener<WebServerInitializedEvent> {

    private static Integer port;

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        port = event.getWebServer().getPort();
    }

    public static ConfigurableApplicationContext run(Class<?> primarySource, String... args) {
        return run(new Class[]{primarySource}, args);
    }

    public static ConfigurableApplicationContext run(Class<?>[] primarySources, String[] args) {
        ConfigurableApplicationContext context = (new SpringApplication(primarySources)).run(args);
        printRunningAt(port);
        return context;
    }


    protected static void printRunningAt(int port) {
        String msg = "\nServer running at:\n";
        msg += " > Local  : http://localhost:" + port+ "\n";

        List<String> ipList = getLocalIpList();
        for (String ip : ipList) {
            msg += " > Network: http://" + ip + ":" + port + "\n";;
        }
        System.out.println(msg);
    }


    private static List<String> getLocalIpList() {
        List<String> ipList = new ArrayList<>();
        try {
            for (Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
                NetworkInterface networkInterface = e.nextElement();
                if (networkInterface.isLoopback() || networkInterface.isVirtual() || !networkInterface.isUp()) {
                    continue;
                }

                for (Enumeration<InetAddress> ele = networkInterface.getInetAddresses(); ele.hasMoreElements();) {
                    InetAddress ip = ele.nextElement();
                    if (ip instanceof Inet4Address) {
                        ipList.add(ip.getHostAddress());
                    }
                }
            }
            return ipList;
        } catch (Exception e) {
            return ipList;
        }
    }
}
