package com.mybatisflex.admin.controller;

import com.mybatisflex.admin.entity.SysOption;
import com.mybatisflex.admin.service.SysOptionService;
import com.mybatisflex.admin.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 控制层。
 *
 * @author michael
 * @since 2023-07-01
 */
@RestController
@RequestMapping("/api/sysOption")
public class SysOptionController {

    @Autowired
    private SysOptionService sysOptionService;

    /**
     * 根据主键更新。
     *
     * @return {@code true} 更新成功，{@code false} 更新失败
     */
    @PostMapping("batchUpdate")
    public Result batchUpdate(@RequestBody Map<String, Object> map) {
        sysOptionService.batchUpdate(map);
        return Result.success();
    }

    /**
     * 查询所有。
     *
     * @return 所有数据
     */
    @GetMapping("all")
    public Result all() {
        Map<String, Object> options = new HashMap<>();
        List<SysOption> sysOptions = sysOptionService.list();
        sysOptions.forEach(sysOption -> {
            Object value = sysOption.getValue();
            if ("true".equalsIgnoreCase((String) value)) {
                value = true;
            } else if ("false".equalsIgnoreCase((String) value)) {
                value = false;
            }
            options.put(sysOption.getKey(), value);
        });
        return Result.success(options);
    }


}