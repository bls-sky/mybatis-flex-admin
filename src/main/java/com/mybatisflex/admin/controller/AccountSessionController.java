package com.mybatisflex.admin.controller;

import com.mybatisflex.core.paginate.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import com.mybatisflex.admin.entity.AccountSession;
import com.mybatisflex.admin.service.AccountSessionService;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;

/**
 *  控制层。
 *
 * @author michael
 * @since 2023-07-01
 */
@RestController
@RequestMapping("/accountSession")
public class AccountSessionController {

    @Autowired
    private AccountSessionService accountSessionService;

    /**
     * 添加。
     *
     * @param accountSession 
     * @return {@code true} 添加成功，{@code false} 添加失败
     */
    @PostMapping("save")
    public boolean save(@RequestBody AccountSession accountSession) {
        return accountSessionService.save(accountSession);
    }

    /**
     * 根据主键删除。
     *
     * @param id 主键
     * @return {@code true} 删除成功，{@code false} 删除失败
     */
    @DeleteMapping("remove/{id}")
    public boolean remove(@PathVariable Serializable id) {
        return accountSessionService.removeById(id);
    }

    /**
     * 根据主键更新。
     *
     * @param accountSession 
     * @return {@code true} 更新成功，{@code false} 更新失败
     */
    @PutMapping("update")
    public boolean update(@RequestBody AccountSession accountSession) {
        return accountSessionService.updateById(accountSession);
    }

    /**
     * 查询所有。
     *
     * @return 所有数据
     */
    @GetMapping("list")
    public List<AccountSession> list() {
        return accountSessionService.list();
    }

    /**
     * 根据主键获取详细信息。
     *
     * @param id 主键
     * @return 详情
     */
    @GetMapping("getInfo/{id}")
    public AccountSession getInfo(@PathVariable Serializable id) {
        return accountSessionService.getById(id);
    }

    /**
     * 分页查询。
     *
     * @param page 分页对象
     * @return 分页对象
     */
    @GetMapping("page")
    public Page<AccountSession> page(Page<AccountSession> page) {
        return accountSessionService.page(page);
    }

}