package com.mybatisflex.admin.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.admin.entity.Account;

/**
 *  映射层。
 *
 * @author michael
 * @since 2023-07-01
 */
public interface AccountMapper extends BaseMapper<Account> {

}
