package com.mybatisflex.admin.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.mybatisflex.annotation.ColumnMask;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.mask.Masks;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *  实体类。
 *
 * @author michael
 * @since 2023-07-07
 */
@Table(value = "tb_account")
public class Account implements Serializable {

    /**
     * 主键ID
     */
    @Id(keyType = KeyType.Auto)
    private BigInteger id;

    /**
     * 登录名
     */
    private String userName;

    /**
     * 密码
     */
    @JSONField(serialize  = false)
    private String password;

    /**
     * 盐
     */
    @JSONField(serialize = false)
    private String salt;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 邮件
     */
    private String email;

    /**
     * 手机电话
     */
    @ColumnMask(Masks.MOBILE)
    private String mobile;

    /**
     * 账户头像
     */
    private String avatar;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 创建日期
     */
    private Date created;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
