package com.mybatisflex.admin.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *  实体类。
 *
 * @author michael
 * @since 2023-07-07
 */
@Table(value = "tb_account_session")
public class AccountSession implements Serializable {

    /**
     * session id
     */
    @Id(keyType = KeyType.Generator, value = "uuid")
    private String id;

    /**
     * 客户端
     */
    private String client;

    /**
     * 账户id
     */
    private BigInteger accountId;

    /**
     * 到期时间
     */
    private Date expireAt;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 扩展属性
     */
    private String options;

    /**
     * 创建时间
     */
    private Date created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public BigInteger getAccountId() {
        return accountId;
    }

    public void setAccountId(BigInteger accountId) {
        this.accountId = accountId;
    }

    public Date getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(Date expireAt) {
        this.expireAt = expireAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
