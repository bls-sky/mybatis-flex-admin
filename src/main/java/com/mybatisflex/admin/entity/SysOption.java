package com.mybatisflex.admin.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *  实体类。
 *
 * @author michael
 * @since 2023-07-07
 */
@Table(value = "tb_sys_option")
public class SysOption implements Serializable {

    
    @Id(keyType = KeyType.Auto)
    private BigInteger id;

    /**
     * 配置key
     */
    private String key;

    /**
     * 配置内容
     */
    private String value;

    /**
     * 创建时间
     */
    @Column(onInsertValue = "now()")
    private Date created;

    /**
     * 修改时间
     */
    @Column(onUpdateValue = "now()")
    private Date modified;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

}
