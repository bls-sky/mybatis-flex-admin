package com.mybatisflex.admin.service;

import com.mybatisflex.core.service.IService;
import com.mybatisflex.admin.entity.SysOption;

import java.util.Map;

/**
 *  服务层。
 *
 * @author michael
 * @since 2023-07-01
 */
public interface SysOptionService extends IService<SysOption> {

    void batchUpdate(Map<String,Object> map);
}