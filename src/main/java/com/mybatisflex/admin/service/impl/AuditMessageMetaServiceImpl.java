package com.mybatisflex.admin.service.impl;

import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.mybatisflex.admin.entity.AuditMessageMeta;
import com.mybatisflex.admin.mapper.AuditMessageMetaMapper;
import com.mybatisflex.admin.service.AuditMessageMetaService;
import org.springframework.stereotype.Service;

/**
 *  服务层实现。
 *
 * @author michael
 * @since 2023-07-01
 */
@Service
public class AuditMessageMetaServiceImpl extends ServiceImpl<AuditMessageMetaMapper, AuditMessageMeta> implements AuditMessageMetaService {

}