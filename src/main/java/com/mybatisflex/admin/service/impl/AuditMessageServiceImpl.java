package com.mybatisflex.admin.service.impl;

import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.mybatisflex.admin.entity.AuditMessage;
import com.mybatisflex.admin.mapper.AuditMessageMapper;
import com.mybatisflex.admin.service.AuditMessageService;
import org.springframework.stereotype.Service;

/**
 *  服务层实现。
 *
 * @author michael
 * @since 2023-07-01
 */
@Service
public class AuditMessageServiceImpl extends ServiceImpl<AuditMessageMapper, AuditMessage> implements AuditMessageService {

}