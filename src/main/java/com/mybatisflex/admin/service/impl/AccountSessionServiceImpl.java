package com.mybatisflex.admin.service.impl;

import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.mybatisflex.admin.entity.AccountSession;
import com.mybatisflex.admin.mapper.AccountSessionMapper;
import com.mybatisflex.admin.service.AccountSessionService;
import org.springframework.stereotype.Service;

/**
 *  服务层实现。
 *
 * @author michael
 * @since 2023-07-01
 */
@Service
public class AccountSessionServiceImpl extends ServiceImpl<AccountSessionMapper, AccountSession> implements AccountSessionService {

}