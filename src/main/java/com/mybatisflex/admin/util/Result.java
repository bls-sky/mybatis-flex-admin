package com.mybatisflex.admin.util;

import java.util.HashMap;

/**
 * @author michael
 */
public class Result extends HashMap<String, Object> {

    private static final String ATTR_ERROR_CODE = "errorCode";
    private static final String ATTR_MESSAGE = "message";
    private static final String ATTR_DATA = "data";


    private Result(int errorCode) {
        put(ATTR_ERROR_CODE,errorCode);
    }


    public static Result success() {
        return new Result(0);
    }

    public static Result create(boolean success) {
        if (success){
            return success();
        }else {
            return error(1);
        }
    }

    public static Result success(Object data) {
        Result ret = new Result(0);
        ret.put(ATTR_DATA, data);
        return ret;
    }

    public static Result success(String key, Object value) {
        Result ret = new Result(0);
        ret.put(key, value);
        return ret;
    }

    public static Result error() {
        return error(1);
    }

    public static Result error(int errorCode) {
        return new Result(errorCode);
    }

    public static Result error(int errorCode, String errorMessage) {
        Result result = error(errorCode);
        result.put(ATTR_MESSAGE, errorMessage);
        return result;
    }


    public int errorCode() {
        return get(ATTR_ERROR_CODE);
    }

    public void errorCode(int errorCode) {
        this.put(ATTR_ERROR_CODE, errorCode);
    }

    public String errorMessage() {
        return this.get(ATTR_MESSAGE);
    }

    public void errorMessage(String errorMessage) {
        put(ATTR_MESSAGE, errorMessage);
    }

    public Object data() {
        return get(ATTR_DATA);
    }

    public void data(Object data) {
        put(ATTR_DATA, data);
    }

    public <T> T get(String key) {
        return (T) super.get(key);
    }

    public Result set(String key, Object value) {
        put(key, value);
        return this;
    }

    public boolean isSuccess() {
        return errorCode() == 0;
    }

}
