import { DEFAULT_LAYOUT } from '../base';
import { AppRouteRecordRaw } from '../types';

const DASHBOARD: AppRouteRecordRaw = {
  path: '/system',
  name: 'system',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '系统设置',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 2,
  },
  children: [
    {
      path: 'settings',
      name: 'settings',
      component: () => import('@/views/dashboard/workplace/index.vue'),
      meta: {
        locale: '系统设置',
        requiresAuth: true,
        roles: ['*'],
      },
    },
  ],
};

export default DASHBOARD;
