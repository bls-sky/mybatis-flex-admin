import { DEFAULT_LAYOUT } from '../base';
import { AppRouteRecordRaw } from '../types';

const DASHBOARD: AppRouteRecordRaw = {
  path: '/account',
  name: 'account',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '用户信息',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 1,
  },
  children: [
    {
      path: 'list',
      name: 'list',
      component: () => import('@/views/dashboard/workplace/index.vue'),
      meta: {
        locale: '账户列表',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'profile',
      name: 'profile',
      component: () => import('@/views/dashboard/workplace/index.vue'),
      meta: {
        locale: '个人资料',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'password',
      name: 'password',
      component: () => import('@/views/dashboard/workplace/index.vue'),
      meta: {
        locale: '修改密码',
        requiresAuth: true,
        roles: ['*'],
      },
    },
  ],
};

export default DASHBOARD;
