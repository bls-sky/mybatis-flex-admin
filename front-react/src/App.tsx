import {RouterProvider} from "react-router-dom";
import {router} from "./routers/router";

import zhCN from 'antd/locale/zh_CN';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';
import React, {useState} from 'react';
import {App as AntdApp, ConfigProvider} from "antd";
import {SettingsContext} from "./context/settings";
import {Locale} from "antd/es/locale";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";

dayjs.locale('zh');

// const queryClient = new QueryClient()

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            //取消窗口获得焦点时，进行查询
            refetchOnWindowFocus: false, // default: true
        },
    },
})

function App() {

    const [lang, setLang] = useState<Locale>(zhCN);

    const settings = {
        setLocale: (locale: Locale) => {
            setLang(locale)
        }
    };


    return (
        <QueryClientProvider client={queryClient}>
            <SettingsContext.Provider value={settings}>
                <ConfigProvider locale={lang}>
                    <AntdApp>
                        <RouterProvider router={router}/>
                    </AntdApp>
                </ConfigProvider>
            </SettingsContext.Provider>
        </QueryClientProvider>
    )
}

export default App
