type ApiResponse = {
    errorCode?: number;
    message?: string;
    data?: any,
};

// 修改 ApiResponse 的 data 类型为 T
type DataResponse<T> = Omit<ApiResponse, 'data'> & { data: T }

type Page = {
    pageNumber: number,
    pageSize: number,
    totalPage: number,
    totalRow: number,
    records: any
}

type AuditMessage = {
    id: number,
    platform: string,
    module: string,
    url: string,
    user: string,
    userIp: string,
    hostIp: string,
    query: string,
    queryParams: string,
    queryTime: string,
    elapsedTime: number,
    created: number,
}

type AccountDTO = {
    id: number,
    userName?: string,
    nickname?: string,
    email?: string,
    mobile?: string,
    avatar?: string,
}