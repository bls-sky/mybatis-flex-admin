import React from "react";
import {App as AntdApp, Button, Checkbox, Form, Input} from "antd";
import styles from "./login.module.scss";
import {useNavigate} from "react-router-dom";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import {useAccountDetail, useLogin} from "../../service/AccountService";
import {useAccountStore} from "../../store/store";

const Login: React.FC = () => {

    const navigate = useNavigate();

    const [form] = Form.useForm();
    const {message} = AntdApp.useApp();
    const login = useLogin();

    const setLogin = useAccountStore((account) =>
        account.setLogin
    );

    const handlerSubmit = async (values: any) => {
        login.mutate({...values}, {
            onSettled: (data, error, variables, context) => {
                if (data?.jwt) {
                    setLogin(data.jwt);

                    message.success("登录成功");
                    navigate("/index")
                } else {
                    message.error(data?.message);
                }
            }
        })
    };

    return (
        <div className={styles.container}>

            <Form
                form={form}
                name="normal_login"
                className="login-form"
                initialValues={{remember: true}}
                onFinish={handlerSubmit}
                style={{
                    width: "400px",
                    height: "400px",
                    marginTop: "15%",
                    background: "#fff",
                    padding: 50,
                    borderRadius: "6px"
                }}
            >

                <h1 style={{marginBottom: '30px'}}> MyBatis-Flex-Admin </h1>

                <Form.Item
                    name="username"
                    rules={[{required: true, message: '账户不能为空'}]}
                    extra="测试账号 admin，密码 123456"
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="账户"/>
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[{required: true, message: '密码不能为空'}]}
                >
                    <Input
                        prefix={<LockOutlined className="site-form-item-icon"/>}
                        type="password"
                        placeholder="密码"
                    />
                </Form.Item>
                <Form.Item>
                    <Form.Item name="remember" valuePropName="checked" noStyle>
                        <Checkbox>记住我</Checkbox>
                    </Form.Item>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button" block
                            loading={login.isLoading}>
                        登 录
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
export default Login;