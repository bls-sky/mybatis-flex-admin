import React, {useState} from 'react';
import {Button, Form, Input, message,} from "antd";
import {Typography} from 'antd';
import { useAccountUpdatePassword} from "../../../service/AccountService";

const {Title} = Typography;


const UpdatePassword: React.FC = () => {
    const [form] = Form.useForm();

    const [submitLoading, setSubmitLading] = useState(false)
    const accountUpdatePwd = useAccountUpdatePassword();
    const onFinish = (values: any) => {
        setSubmitLading(true)
        accountUpdatePwd.mutate(values, {
            onSettled: (data, error, variables, context) => {
                setSubmitLading(false);
                if (data && data.errorCode == 0 && !data.message){
                    message.success("密码修改成功")
                }
            }
        });
    };

    const handleSubmit = () => {
        form.submit();
    }


    return (
        <>
            <Title level={4} style={{paddingBottom: '20px'}}>修改密码</Title>
            <Form
                layout="vertical"
                form={form}
                style={{maxWidth: 600}}
                onFinish={onFinish}
            >
                <Form.Item label="旧密码" name="oldPwd">
                    <Input placeholder="请输入旧密码" type={"password"}/>
                </Form.Item>
                <Form.Item label="新密码" name="newPwd">
                    <Input placeholder="请输入新密码" type={"password"}/>
                </Form.Item>
                <Form.Item label="确认密码" name="confirmPwd">
                    <Input placeholder="请输入确认密码" type={"password"}/>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" loading={submitLoading} onClick={() => handleSubmit()}>提交</Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default UpdatePassword;
