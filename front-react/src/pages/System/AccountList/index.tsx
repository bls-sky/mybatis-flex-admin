import React, {useState} from 'react';
import {
    ColumnHeightOutlined, DeleteOutlined, DownloadOutlined,
    EditOutlined, FormatPainterOutlined,
    ReloadOutlined,
    SwapOutlined,
} from '@ant-design/icons';
import {
    Button,
    Space,
    Table,
    Popconfirm,
} from 'antd';
import {ColumnsType} from "antd/es/table";
import {TableRowSelection} from "antd/es/table/interface";
import AccountDetail from "./AccountDetail";
import SearchForm from "./SearchForm";
import {useSearchParams} from "react-router-dom";
import {useAccountDelete, useAccountPage} from "../../../service/AccountService";


const AccountList: React.FC = () => {
    const [searchParams, setSearchParams] = useSearchParams();


    const pageParams = {pageSize: 10} as any;
    searchParams.forEach((value, key) => {
        pageParams[key] = value;
    })

    const {isLoading, data} = useAccountPage(pageParams);


    const [editId, setEditId] = useState<number>();
    const [isModalOpen, setIsModalOpen] = useState(false);

    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const [selectCount, setSelectCount] = useState(0);

    const selectNone = () => {
        setSelectedRowKeys([]);
        setSelectCount(0);
    };

    // rowSelection object indicates the need for row selection
    const rowSelection = {
        selectedRowKeys,
        onChange: (selectedRowKeys: React.Key[], selectedRows: any) => {
            setSelectedRowKeys([...selectedRowKeys]);
            setSelectCount(selectedRows.length);
        },
        getCheckboxProps: (record: any) => ({
            disabled: record.name === 'Disabled User', // Column configuration not to be checked
            name: record.name,
        }),
    };

    const onSearch = (values: any) => {
        setSearchParams(values);
    }

    const onChange = (pagination: number) => {
        setSearchParams({
            ...pageParams,
            pageNumber: pagination,
        })
    }


    function handleEdit(id: number) {
        setEditId(id);
        setIsModalOpen(true);
    }

    const accountDelete = useAccountDelete();

    function handleDelete(id: number) {
        accountDelete.mutate(id)
    }

    const columns: ColumnsType<AccountDTO> = [
        {
            title: '用户名',
            dataIndex: 'userName',
            key: 'userName',
        },
        {
            title: '邮箱',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: '手机号',
            dataIndex: 'mobile',
            key: 'mobile',
        },
        {
            title: '操作',
            fixed: 'right',
            width: 150,
            render: (_: any, record: AccountDTO) => {
                return (
                    <Space>
                        <a onClick={() => handleEdit(record.id)}><EditOutlined/> 编辑</a>
                        <Popconfirm
                            title="确定删除"
                            description="您确定要删除该账户吗?"
                            okText="确定"
                            cancelText="取消"
                            placement="topLeft"
                            onConfirm={() => handleDelete(record.id)}
                        >
                            <a style={{color: 'red'}}><DeleteOutlined/> 删除</a>
                        </Popconfirm>
                    </Space>
                )
            }
        },
    ];

    return (
        <div>
            <SearchForm onSearch={onSearch}/>

            <AccountDetail title="创建用户"
                           open={isModalOpen}
                           onOk={() => {
                               setIsModalOpen(false)
                           }}
                           onCancel={() => {
                               setEditId(0)
                               setIsModalOpen(false)
                           }}
                           editId={editId}
            />

            <Space style={{display: "flex", justifyContent: "space-between", padding: "10px 0"}}>

                <Space align={"center"}>

                    <Button type="primary" onClick={() => {
                        setEditId(0);
                        setIsModalOpen(!isModalOpen)
                    }}>新增</Button>

                    {selectCount > 0 &&
                    <div style={{
                        border: "1px solid #abdcff",
                        borderRadius: "6px",
                        padding: "0 10px",
                        margin: "-1px",
                        background: "#f0faff"
                    }}>
                        <Space>
                            <div>
                                已选择 {selectCount} 项
                            </div>

                            <Button type="link" danger>
                                <DeleteOutlined/>全部删除
                            </Button>

                            <Button type="link" onClick={selectNone}>取消选择</Button>
                        </Space>
                    </div>
                    }


                </Space>

                <Space align={"center"} size={"middle"}>
                    <ReloadOutlined/>
                    <DownloadOutlined/>
                    <ColumnHeightOutlined/>
                    <FormatPainterOutlined/>
                    <SwapOutlined/>
                </Space>

            </Space>

            <Table loading={isLoading}
                   dataSource={data?.records || []}
                   rowKey={(record) => record.id}
                   columns={columns}
                   rowSelection={{
                       type: 'checkbox',
                       ...rowSelection,
                   } as TableRowSelection<any>}

                   pagination={
                       {
                           showQuickJumper: true,
                           defaultCurrent: data?.pageNumber || 1,
                           total: data?.totalRow || 0,
                           showTotal: (total) => `共 ${total} 条数据`,
                           onChange
                       }
                   }
            />
        </div>
    )
}
export default AccountList