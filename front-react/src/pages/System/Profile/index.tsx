import React, {useEffect, useState} from 'react';
import {Button, Col, Form, Input, message, Row, Upload, UploadProps} from "antd";
import {Typography} from 'antd';
import {UploadOutlined} from "@ant-design/icons";
import {useAccountDetail, useAccountUpdate} from "../../../service/AccountService";

const baseUrl = `${import.meta.env.VITE_APP_SERVER_ENDPOINT}/api/`;
const authKey = `${import.meta.env.VITE_APP_AUTH_KEY || "authKey"}`;

const {Title} = Typography;

const Profile: React.FC = () => {

    const [form] = Form.useForm();
    const {isLoading, data, refetch} = useAccountDetail();

    useEffect(() => {
        if (!isLoading) {
            form.setFieldsValue(data!.account);
        }
    }, [isLoading])


    const props: UploadProps = {
        name: 'file',
        action: baseUrl + 'account/update/avatar',
        headers: {
            Authorization: localStorage.getItem(authKey) || "",
            Accept: "Application/json",
        },
        onChange(info) {
            if (info.file.status === 'done' && info.file.response.errorCode === 0) {
                refetch();
            }
        },
    };

    const handleSubmit = () => {
        form.submit();
    }


    const [submitLoading, setSubmitLading] = useState(false)
    const accountUpdate = useAccountUpdate();
    const onFinish = (values: any) => {
        setSubmitLading(true)
        accountUpdate.mutate(values, {
            onSettled: (data, error, variables, context) => {
                setSubmitLading(false);
                message.success('修改成功')
            }
        });
    };


    return (
        <>
            <Title level={4} style={{paddingBottom: '20px'}}>个人资料</Title>
            <Row>
                <Col span={12}>
                    <Form
                        layout="vertical"
                        form={form}
                        style={{maxWidth: 600}}
                        onFinish={onFinish}
                    >
                        <Form.Item name="id" hidden={true}>
                            <Input type={"hidden"}/>
                        </Form.Item>
                        <Form.Item label="登录账户" name="userName">
                            <Input disabled={true}/>
                        </Form.Item>
                        <Form.Item label="昵称" name="nickname">
                            <Input placeholder="请输入您的昵称"/>
                        </Form.Item>
                        <Form.Item label="邮箱" name="email">
                            <Input placeholder="请输入邮箱"/>
                        </Form.Item>
                        <Form.Item label="手机号" name="mobile">
                            <Input placeholder="请输入手机号码"/>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" loading={submitLoading} onClick={() => handleSubmit()}>提交</Button>
                        </Form.Item>
                    </Form>
                </Col>

                <Col span={12} style={{display: "flex"}}>
                    <div style={{margin: "auto"}}>
                        <div>
                            <img src={data?.account.avatar}
                                 style={{width: "200px", height: "200px", borderRadius: "50%"}}/>
                        </div>
                        <div style={{padding: "20px 0 0 40px"}}>
                            <Upload {...props}>
                                <Button icon={<UploadOutlined/>}>上传新头像</Button>
                            </Upload>
                        </div>
                    </div>
                </Col>

            </Row>


        </>
    );
};

export default Profile;
