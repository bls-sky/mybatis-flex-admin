import {
    useMutation,
    UseMutationOptions,
    useQuery, useQueryClient,
} from "@tanstack/react-query";
import axios, {AxiosError} from "axios";




export const useOptionsUpdate = (
    options: UseMutationOptions<ApiResponse, AxiosError<ApiResponse>, AccountDTO> = {}
) => {
    const queryClient = useQueryClient();

    return useMutation(
        async (account) => {
            const resp = await axios.post('/sysOption/batchUpdate', account).catch((err) => {
                return err;
            });
            return resp.data ? resp.data : resp;
        },
        {
            ...options,
            onSuccess: (data, payload, ...args) => {
                //移除缓存
                queryClient.removeQueries({
                    queryKey: ["useOptionsQuery"]
                });

                if (options.onSuccess) {
                    options.onSuccess(data, payload, ...args);
                }
            }
        }
    );
}



export const useOptionsQuery = () => {
    return useQuery(["useOptionsQuery"], async () => {
        const resp = await axios.get('/sysOption/all').catch((err) => {
            return err;
        });
        return resp.data ? resp.data : resp;
    })
}
