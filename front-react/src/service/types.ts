export interface User{
    userName:string,
    password:string,
}

export interface Page<T>{
    pageNumber:number,
    pageSize:number,
    totalPage:number,
    totalRow:number,
    records:T[],
}


export interface AuditMessage {
    /**
     * 主键
     */
    id: number;

    /**
     * 平台，或者是应用
     */
    platform: string;

    /**
     * 模块
     */
    module: string;

    /**
     * url id
     */
    url: string;

    /**
     * 平台用户
     */
    user: string;

    /**
     * 平台用户 IP 地址
     */
    userIp: string;

    /**
     * 服务器 IP
     */
    hostIp: string;

    /**
     * 执行的 sql
     */
    query: string;

    /**
     * 执行的 sql 的参数
     */
    queryParams: string;

    /**
     * 执行时间
     */
    queryTime: string;

    /**
     * 执行消耗时间
     */
    elapsedTime: number;

    /**
     * 创建时间
     */
    created: string;
}